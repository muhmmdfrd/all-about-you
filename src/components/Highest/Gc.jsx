import React from "react";
import MyPhoto1 from "../../../static/img/gc-1.jpg";
import MyPhoto2 from "../../../static/img/gc-2.jpg";

const Gc1 = () => <img width={300} src={MyPhoto1} />;
const Gc2 = () => <img width={300} src={MyPhoto2} />;

export { Gc1, Gc2 };
