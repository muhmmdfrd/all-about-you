import React from "react";
import MyPhoto from "../../../static/img/week-after-breakup.jpg";

export const PhotoAfterBreakup = () => <img width={400} src={MyPhoto} />;
