import React from "react";
import clsx from "clsx";
import styles from "./styles.module.css";

type FeatureItem = {
  title: string;
  Svg: React.ComponentType<React.ComponentProps<"svg">>;
  description: JSX.Element;
};

const FeatureList: FeatureItem[] = [
  {
    title: "Apa Isi Website Ini?",
    Svg: require("@site/static/img/undraw_docusaurus_mountain.svg").default,
    description: (
      <>
        Semua yang aku rasakan dan lewati beberapa bulan terakhir ini aku
        tuliskan disini. Karena ini website, kamu bisa kapan saja untuk
        membacanya.
      </>
    ),
  },
  {
    title: "Mengapa Aku Membuat Web?",
    Svg: require("@site/static/img/undraw_docusaurus_tree.svg").default,
    description: (
      <>
        Aku membuat web ini karena aku ingin mengenal lebih jauh tentangmu.
        Namun, aku sangat paham kesibukanmu sehingga kita tidak bisa
        berkomunikasi secara intens. Bukankah kamu ingin dengar cerita aku? Yups
        aku tuliskan disini.
      </>
    ),
  },
  {
    title: "Kapan Aku Membuat Website Ini?",
    Svg: require("@site/static/img/undraw_docusaurus_react.svg").default,
    description: (
      <>
        Aku mulai membuat website ini per tanggal 30 Januari 2023, selingan
        sambil kerja. Aku rencananya karena libur kuliah, aku bisa menulis
        ceritanya di malam hari selepas bekerja.
      </>
    ),
  },
];

function Feature({ title, Svg, description }: FeatureItem) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
