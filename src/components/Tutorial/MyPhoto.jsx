import React from "react";
import MyPhoto from "../../../static/img/me.jpg";

export const MyProfile = () => <img width={200} src={MyPhoto} />;
